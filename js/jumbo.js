/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function() {

    $("#IRSensorSlider").slider({
        orientation: "vertical",
        range: "min",
        min: 0,
        max: 1023,
        value: 500,
        slide: function(event, ui) {

            var value = ui.value;
            $("#label").innerHTML = value;

            if (value > 800) {
                $("#behaviorBar2").css('visibility', 'visible');
                $("#behaviorBar0").css('visibility', 'hidden');
            }
            else {
                $("#behaviorBar2").css('visibility', 'hidden');
                $("#behaviorBar0").css('visibility', 'visible');
            }
        }
    });
    
        
    $("#bumper")
            .mouseup(function() {
                
                $("#behaviorBar3").css('visibility', 'hidden');
        
        
            })
            .mousedown(function() {
                $("#behaviorBar3").css('visibility', 'visible');
                $("#behaviorBar0").css('visibility', 'hidden');
                $("#behaviorBar2").css('visibility', 'hidden');
            });
           
   
    $("input[type=submit], a, button").button();

});